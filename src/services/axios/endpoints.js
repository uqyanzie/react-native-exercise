export default {
  country: {
    getAll: {
      method: 'get',
      url: '/all?',
    },
    getByName: {
      method: 'get',
      url: '/name/:id',
    },
  },
};
