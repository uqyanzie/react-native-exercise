import { api } from '../../api';
export const pokemonApi = api.injectEndpoints({
  endpoints: build => ({
    searchPokemon: build.query({
      query: (keyword) => `/pokemon/${keyword}/`
    }),
  }),
  overrideExisting: false,
});
export const { useLazySearchPokemonQuery } = pokemonApi;
