import { api } from '../../api';
export const userApi = api.injectEndpoints({
  endpoints: build => ({
    login: build.query({
      query: () => '/auth/login',
    }),
    todo: build.query({
      query: (id) => `/todos/${id}`
    }),
  }),
  overrideExisting: false,
});
export const { useLazyLoginQuery, useLazyTodoQuery } = userApi;
