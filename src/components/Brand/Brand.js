import React from 'react';
import { View, Image, Text } from 'react-native';
import { useTheme } from '../../hooks';
import { splash_img } from '../../assets';

const Brand = ({ height, width, mode }) => {
  const { Layout } = useTheme();
  return (
    <View testID={'brand-img-wrapper'} style={{ height, width }}>
      <Image
        testID={'brand-img'}
        style={Layout.fullSize}
        source={splash_img}
        resizeMode={mode}
      />
      <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 20}}>
        Countries.db
      </Text>
    </View>
  );
};

Brand.defaultProps = {
  height: 300,
  width: 300,
  mode: 'contain',
};

export default Brand;
