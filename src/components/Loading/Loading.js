import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import { useTheme } from '../../hooks';

const Loading = ({ size }) => {
  const { Layout, Gutters, Colors } = useTheme();
  return (
    <View style={[ Layout.center, Gutters.largeTMargin ]}>
      <ActivityIndicator size={size} color={Colors.primary} />
    </View>
  );
};

Loading.defaultProps = {
  size: 'large',
};

export default Loading;
