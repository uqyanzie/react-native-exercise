import * as React from 'react';
import Experiences from '../screens/Experiences';
import Skills from '../screens/Skills';
import Profile from '../screens/Profile';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

import { useTheme } from '../hooks';

const materialCommunityIcons = code => {
  return <MaterialCommunityIcons name={code} size={30} color="#900" />;
};

const Tab = createBottomTabNavigator();
// @refresh reset
const MainNavigator = () => {
  const { Layout, darkMode, NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: colors.primary,
      }}
      initialRouteName='Profile'
    >
      <Tab.Screen
        name="Experiences"
        component={Experiences}
        options={{
          headerShown: true,
          tabBarLabel: 'Experiences',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="star-shooting-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Skills"
        component={Skills}
        options={{
          headerShown: true,
          tabBarLabel: 'Skills',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="dumbbell" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          headerShown: true,
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default MainNavigator;
