import React, { useEffect } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  TextInput,
  Image,
  Text,
} from 'react-native';
import { useTheme } from '../../hooks';
import { API } from '../../services/axios/api';
import { debounce } from 'lodash';

const Countries = ({ navigation }) => {
  const { Layout, Gutters } = useTheme();
  const [searchName, setSearchName] = React.useState('');
  const [countryList, setCountryList] = React.useState([]);
  const [errorMessage, setErrorMessage] = React.useState(null);

  const getAllCountries = async () => {
    await API('country.getAll')
      .then(response => {
        setCountryList(response.data);
        setErrorMessage(null);
      })
      .catch(error => {
        setCountryList([]);
        setErrorMessage(error.ErrorMsg);
      });
  };

  const getCountryByName = debounce(async () => {
    await API('country.getByName', {
      query: { id: searchName },
    })
      .then(response => {
        setCountryList(response.data);
        setErrorMessage(null);
      })
      .catch(error => {
        setCountryList([]);
        setErrorMessage(error.ErrorMsg);
      });
  }, 1000);

  useEffect(() => {
    if (searchName.length > 3) {
      getCountryByName();
    } else {
      getAllCountries();
      console.log(countryList);
      console.log(errorMessage);
    }
  }, [searchName]);

  return (
    <ScrollView
      style={Layout.fill}
      contentContainerStyle={[
        Layout.fullSize,
        Layout.fill,
        Layout.colCenter,
        Layout.scrollSpaceBetween,
      ]}
    >
      <View
        style={[
          Layout.fill,
          Layout.relative,
          Layout.fullWidth,
          Layout.justifyContentCenter,
          Layout.alignItemsCenter,
        ]}
      >
        <Text style={{ fontSize: 18, fontWeight: 700 }}>Countries Database</Text>
        <View style={[Gutters.tinyVMargin]} />
        <TextInput
          style={[styles.input, {width: 370}]}
          onChangeText={setSearchName}
          value={searchName}
          placeholder="Search Country"
        />
        <View style={[Gutters.LargeVMargin, {marginHorizontal: 50}]} />
        <ScrollView style={[{width: 380}]}>
          {countryList.length > 0 &&
            countryList.map(item => {
              return (
                <View key={item.name.common} 
                  style={[{marginVertical: 10, flexDirection:'row', backgroundColor: '#fff', elevation: 4, shadowColor: '#52006A', paddingVertical: 10}]}>
                  <Image
                      style={[styles.flag, {marginLeft: 20}]}
                      source={{
                        uri: item.flags.png,
                      }}
                    />
                  <View style={{marginLeft: 10, justifyContent: 'center'}}>
                    <Text
                      style={{ fontWeight: 700 }}
                      >{`${item.name.common}`}
                    </Text>
                    <Text>{`Capital City : ${item.capital}`}</Text>
                  </View>
                </View>
              );
            })}
        </ScrollView>
        <View style={[Gutters.tinyVMargin]} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  flag: {
    width: 100,
    height: 60,
  },
});

export default Countries;
