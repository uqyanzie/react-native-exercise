import React, { useState, setState } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
} from 'react-native';
import { useTheme } from '../../hooks';
import Slider from '@react-native-community/slider';

const Skills = ({ navigation }) => {
  
  const { Layout, Gutters, Colors, Fonts } = useTheme();

  const minValue = 0;
  const maxValue = 10;

  const sliderLabel = [];
  sliderLabel.push(<Text style={{color:'#ffffff'}} hide>{minValue}</Text>);
  
  for(let i = minValue+1; i < maxValue; i++){
    sliderLabel.push(
      <Text style={[styles.colorBlack, {fontWeight: 'bold'}]}>{i}</Text>
    )
  }
  sliderLabel.push(<Text style={{color:'#ffffff'}} hide>{maxValue}</Text>);

  const SkillSlider= props => {
    return (
      <View style={{borderColor: Colors.primary, borderWidth: 1, marginVertical: 10, paddingVertical: 10}}>
        <Text style={{marginLeft: 18, fontWeight: 'bold'}}>{props.skillName}</Text>
        <Slider
          style={{ width: 350}}
          step={1}
          minimumValue={minValue}
          maximumValue={maxValue}
          value={props.value}
          thumbTintColor={Colors.primary}
          maximumTrackTintColor='#454545' 
          minimumTrackTintColor={Colors.primary}
        />
        <View style={[styles.textCon, Layout.justifyContentAround]}>
            {sliderLabel}
        </View>
      </View>
    )
  }
    
  
  return (
    <ScrollView style={{backgroundColor:'white', paddingTop:20}}>
      <Text style={[Fonts.titleSmall, {textAlign: 'center'}]}>
        Hard Skills
      </Text>
      <View style={[Layout.colCenter, {width: 400, marginBottom: 20}]}>
        <SkillSlider skillName='Scientific Writing' value={7}/>
        <SkillSlider skillName='Technical Writing' value={8}/>
        <SkillSlider skillName='Front End Development (VueJs, Flutter)' value={5}/>
        <SkillSlider skillName='Back End Development (Laravel, CodeIgniter)' value={6}/>
        <SkillSlider skillName='System Analysis' value={7}/>
      </View>
      <Text style={[Fonts.titleSmall, {textAlign: 'center'}]}>
        Soft Skills
      </Text>
      <View style={[Layout.colCenter, {width: 400, marginBottom: 20}]}>
        <SkillSlider skillName='English' value={9}/>
        <SkillSlider skillName='Communication' value={7}/>
        <SkillSlider skillName='Teamwork' value={8}/>
        <SkillSlider skillName='Management' value={8}/>
        <SkillSlider skillName='Leadership' value={7}/>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#000',
  },
  textCon: {
      flexDirection: 'row',
      marginLeft: -6,
      width: 370
  },
  colorBlack: {
      color: '#000000'
  },
  colorYellow: {
      color: 'rgb(252, 228, 149)'
  }
});

export default Skills;
