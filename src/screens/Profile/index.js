import React, { useEffect } from 'react';
import { ScrollView, View, Text, TextInput, Button, Alert, Image } from 'react-native';
import { useTheme } from '../../hooks';
import { useDispatch } from 'react-redux';
import { removeToken } from '../../store/auth';
import { profile_img } from '../../assets';
import { border } from 'native-base/lib/typescript/theme/styled-system';
import { color } from 'react-native-reanimated';

const Profile = ({ navigation }) => {
  const { Common, Layout, Gutters, Fonts, darkMode, Colors, FontSize } = useTheme();
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(removeToken());
    navigation.reset({
      index: 0,
      routes: [{ name: 'Login' }],
    });
  };

  const handleFetch = () => {
    console.log('FETCH');
  };

  return (
    <ScrollView
      style={[Layout.fill, {backgroundColor:'white'}]}
      contentContainerStyle={[
        Layout.fullSize,
        Layout.fill,
        Layout.colCenter,
        Layout.scrollSpaceBetween,
      ]}
    >
      <View
        style={[
          Layout.fill,
          Layout.relative,
          Layout.fullWidth,
          Gutters.smallPadding, 
          //{justifyContent: 'space-around'}
        ]}
      >
        <Text style={[{textAlign:'center', marginBottom: 10}, Fonts.titleSmall]}>
          My Profile
        </Text>
        <View style={[{
          paddingVertical: 10, 
          paddingHorizontal: 10},
          Layout.justifyContentCenter,
          Layout.alignItemsCenter]}>
            <Image
              style={{
                borderRadius: 50,
                height: 100,
                width: 100,
              }}
              source={profile_img}
            />
            <View style={[{
              marginVertical: 10,
              },]}>
              <Text style={{
                fontSize: FontSize.regular, 
                fontWeight: 'bold',
                textAlign: 'center'
                }}>Uqyanzie Bintang</Text>
              <Text style={{textAlign: 'center'}}>uqyanzie@gmail.com</Text>
            </View>
        </View>
        <View style={{
          borderColor: Colors.primary, 
          borderWidth:2, 
          borderRadius: 10, 
          paddingHorizontal: 2,
          paddingTop: 15,
          paddingBottom: 20, 
          marginBottom: 80,
          alignItems: 'center'
        }}>
          <Text style={[
            Fonts.titleSmall, 
            Fonts.textCenter,
            ] 
          }>
          About Me
          </Text>
          <View style={{borderBottomColor: Colors.primary, borderBottomWidth: 1, marginTop: 5, marginBottom: 15, width: 200,}}></View>
          <View style={[
            Layout.alignItemsCenter, 
            {marginVertical:10}]
          }>
            <Text style={[
              Fonts.textBold, 
              Fonts.textCenter,
              Layout.justifyContentBetween, 
              {fontSize: 16}]
            }>
            Tempat, Tanggal Lahir
            </Text>
            <Text style={{fontSize: 14}}>Bandung, 14 April 2003</Text>
          </View>
          <View style={[
            Layout.alignItemsCenter, 
            {marginVertical:10}]
          }>
            <Text style={[
              Fonts.textBold, 
              Fonts.textCenter,
              {fontSize: 16}]
            }>
            Hobby
            </Text>
            <Text style={{fontSize: 14}}>Music & Travelling</Text>
          </View>
          <View style={[
            Layout.alignItemsCenter, 
            {marginVertical:10}]
          }>
            <Text style={[Fonts.textBold,{fontSize: 16}]}>Quotes of Life</Text>
            <Text style={{fontSize: 14}}>Work hard Pray hard</Text>
          </View>
          <View style={[
            Layout.alignItemsCenter, 
            {marginVertical:10}]
          }>
            <Text style={[Fonts.textBold,{fontSize:16}]}>About Myself</Text>
            <Text style={{fontSize: 14}}>College Student in Politeknik Negeri Bandung</Text>
          </View>
        </View>
        <Button title="Logout" style={{marginTop:50, justifyContent: 'flex-end'}} onPress={() => handleLogout()} color={Colors.primary}/>  
      </View>
    </ScrollView>

    
  );
};
export default Profile;
