import React, { useEffect, useState } from 'react';
import { ScrollView, View, TextInput, Alert, Image,  } from 'react-native';
import { useTheme } from '../../hooks';
import { useDispatch } from 'react-redux';
import { removeToken } from '../../store/auth';
import { Button, useColorMode, Center, Text } from 'native-base';
import { useLazyLoginQuery, useLazyTodoQuery } from '@/services/modules/users';
import {splash_img} from '@/assets'

const Dashboard = ({ navigation }) => {
  const { Common, Layout, Gutters, Colors, Fonts, darkMode } = useTheme();
  const dispatch = useDispatch();
  const [userId, setUserId] = useState();

  const { colorMode, toggleColorMode } = useColorMode();

  const handleLogout = () => {
    dispatch(removeToken());
    navigation.reset({
      index: 0,
      routes: [{ name: 'Login' }],
    });
  };

  const [
    todo,
    { data, isSuccess, isLoading, isFetching, error },
  ] = useLazyTodoQuery()

  useEffect(() => {
      if (data)
        setUserId(data.userId)
      else
        setUserId()
  }, [data, isSuccess, isLoading, isFetching, error]);

  const handleFetch = () => {
    setUserId()
    console.log('FETCH');
    todo(1);

    // if (data) {
    //   setUserId(data.userId)
    // }
  };

  const handleVoice2Text = () => {
    navigation.navigate("Voice to Text")
  };

  return (
    <ScrollView
      style={Layout.fill}
      contentContainerStyle={[
        Layout.fullSize,
        Layout.fill,
        Layout.colCenter,
        Layout.scrollSpaceBetween,
      ]}
    >
      <View
        style={[
          Layout.fill,
          Layout.relative,
          Layout.fullWidth,
          Gutters.smallPadding,
          Layout.center
        ]}
      >
      {/* <Center
        flex="1"
        p="8"
        bg={colorMode === 'dark' ? 'coolGray.800' : 'warmGray.50'}
      > */}
        <View style={[Layout.center]}>
          <Image style={{width: 200, height: 200}} source={splash_img}/>
        </View>
        <Text style={[darkMode ? [Fonts.textWhite, Fonts.text600] : [Fonts.text600, {color: '#000000'}]]}>Welcome to Countries.db Dashboard</Text>
        <View style={[Gutters.tinyVMargin]} />
        <View style={[Gutters.tinyVMargin]} />
        <Button style={Layout.fullWidth} onPress={() => handleFetch()} isLoading={isFetching} bgColor={Colors.primary}>Fetch</Button>
        {
          (!isFetching) ?
            <Text style={{ color: Colors.primary }}>{userId}</Text>
          : <Text style={{ color: Colors.primary }}></Text>
        }
        <View style={[Gutters.tinyVMargin]} />
        <Button style={Layout.fullWidth} onPress={() => toggleColorMode()} bgColor={Colors.primary}>
          {colorMode === 'dark' ? 'Change to Light' : 'Change to Dark'}
        </Button>
      {/* </Center> */}
      </View>
    </ScrollView>
  );
};
export default Dashboard;
