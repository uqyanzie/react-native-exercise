import {View, StyleSheet} from 'react-native';
import { useTheme } from '../../hooks';
import { useColorMode, Center, Text } from 'native-base';
import Timeline from 'react-native-timeline-flatlist';

const Experiences = ({ navigation }) => {
  const { Common, Layout, Gutters, Colors, Fonts, darkMode } = useTheme();
  const { colorMode, toggleColorMode } = useColorMode();

  const data = [
    {time: '2023', title: 'Software Engineer Intern', description: 'Ongoing from June 2023', circleColor: '#009688',lineColor:'#009688'},
    {time: '2023', title: 'Develop a Web Based Monitoring System for a Farm Environment', description: 'Ongoing from May 2023'},
    {time: '2023', title: 'Senior Staff at Himpunan Mahasiswa Komputer Politeknik Negeri Bandung', description: 'Ongoing from March 2023'},
    {time: '2023', title: 'Deputy of Polban Mengajar Headmaster', description: 'Ongoing from January 2023'},
    {time: '2022', title: 'Participating in Gemastik 2022 on The Competitive Programming', description: 'October 2022', lineColor:'#009688'},
    {time: '2021', title: 'Top 15 Participant at HIMIT PENS Web Design Competition', description: 'September 2021', circleColor: '#009688'},
    {time: '2021', title: 'Graduated from SMAN 11 Bandung', description: 'June 2021', circleColor: '#009688',lineColor:'#009688'},
    {time: '2019', title: 'Member of Himpunan Siswa Pecinta Alam SMAN 11 Bandung', description: '2019 - 2021'},
    {time: '2018', title: 'Graduated From SMPN 13 Bandung', description: 'July 2018'},
  ]

  return (
    <View style={styles.container}>
        <Timeline 
          style={styles.list}
          data={data}
          circleSize={28}
          dotSize={15}
          circleColor='rgb(45,156,219)'
          lineColor='rgb(45,156,219)'
          timeContainerStyle={{minWidth:52, marginTop: 0}}
          timeStyle={{textAlign: 'center', backgroundColor:'#ff9797', color:'white', padding:5, borderRadius:13, marginRight:5}}
          descriptionStyle={{color:'gray'}}
          options={{
            style:{paddingTop:5}
          }}
          innerCircle={'dot'}
          isUsingFlatlist={true}
          separator={true}
        />
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingRight:20,
    paddingLeft: 15,
		paddingTop:3,
    backgroundColor:'white'
  },
  list: {
    flex: 1,
    marginTop:10,
  },
});

export default Experiences;
